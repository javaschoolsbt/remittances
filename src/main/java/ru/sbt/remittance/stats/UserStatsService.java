package ru.sbt.remittance.stats;

import ru.sbt.remittance.model.User;

public interface UserStatsService {
    UserStats getUserStats(User user);
}
