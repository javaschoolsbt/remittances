package ru.sbt.remittance.stats;

import ru.sbt.remittance.model.Text;
import ru.sbt.remittance.model.User;

public class UserStats {

    private final User user;
    private final Iterable<Text> texts;
    private final long cardsCount;
    private final long cardsMinded;
    private final long cardsLearning;

    public UserStats(User user, Iterable<Text> texts, long cardsCount, long cardsMinded) {
        this.user = user;
        this.texts = texts;
        this.cardsCount = cardsCount;
        this.cardsMinded = cardsMinded;
        this.cardsLearning = cardsCount - cardsMinded;
    }

    public User getUser() {
        return user;
    }

    public Iterable<Text> getTexts() {
        return texts;
    }

    public long getCardsCount() {
        return cardsCount;
    }

    public long getCardsMinded() {
        return cardsMinded;
    }

    public long getCardsLearning() {
        return cardsLearning;
    }
}
