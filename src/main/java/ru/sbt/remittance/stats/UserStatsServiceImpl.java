package ru.sbt.remittance.stats;

import org.springframework.stereotype.Service;
import ru.sbt.remittance.model.Text;
import ru.sbt.remittance.model.User;
import ru.sbt.remittance.repository.TextRepository;
import ru.sbt.remittance.repository.VocabularyCardRepository;

@Service
public class UserStatsServiceImpl implements UserStatsService {

    private final TextRepository textRepository;
    private final VocabularyCardRepository cardRepository;

    public UserStatsServiceImpl(TextRepository textRepository, VocabularyCardRepository cardRepository) {
        this.textRepository = textRepository;
        this.cardRepository = cardRepository;
    }

    @Override
    public UserStats getUserStats(User user) {
        Iterable<Text> texts = textRepository.findByUserId(user.getId());
        long cardsCount = cardRepository.countByUserId(user.getId());
        long cardsMinded = cardRepository.countByUserIdAndMindedIsTrue(user.getId());
        return new UserStats(user, texts, cardsCount, cardsMinded);
    }
}
