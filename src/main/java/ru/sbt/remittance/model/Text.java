package ru.sbt.remittance.model;

import javax.persistence.*;

@Entity
@Table(name = "TEXTS")
public class Text {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String topic;

    @Column(length = 2000)
    private String documentText;

    @ManyToOne(cascade = CascadeType.MERGE)
    private User user;

    protected Text() {
    }

    public long getId() {
        return id;
    }

    public String getTopic() {
        return topic;
    }

    public String getDocumentText() {
        return documentText;
    }

    public User getUser() {
        return user;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public void setDocumentText(String documentText) {
        this.documentText = documentText;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
