package ru.sbt.remittance.dao;

import ru.sbt.remittance.model.User;

import java.util.List;

public interface UserDao {
    User create(String login, String password, String firstName, String lastName) throws DuplicateUsernameException;

    User findByUsername(String login);

    List<User> getAll();
}
