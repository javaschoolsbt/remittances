package ru.sbt.remittance.web;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.sbt.remittance.model.User;
import ru.sbt.remittance.model.VocabularyCard;
import ru.sbt.remittance.repository.VocabularyCardRepository;
import ru.sbt.remittance.services.UserService;

@Controller
@RequestMapping("/cards")
public class VocabularyCardController {
    private final VocabularyCardRepository cardRepository;
    private final UserService userService;

    public VocabularyCardController(VocabularyCardRepository cardRepository, UserService userService) {
        this.cardRepository = cardRepository;
        this.userService = userService;
    }

    @PostMapping
    @ResponseBody
    @Transactional
    public long create(String word) {
        User currentUser = userService.getCurrentUser();
        VocabularyCard card = cardRepository.findOneByUserIdAndWord(currentUser.getId(), word);
        if (card == null) {
            card = new VocabularyCard(word, "", false, currentUser);
            return cardRepository.save(card).getId();
        } else return card.getId();
    }

    @GetMapping
    public ModelAndView list() {
        Iterable<VocabularyCard> cards = this.cardRepository.findByUserId(userService.getCurrentUser().getId());
        return new ModelAndView("cards/list", "cards", cards);
    }

    @GetMapping(value = "remember/{id}")
    public String remember(@PathVariable("id") VocabularyCard card) {
        if (card.getUser().getLogin().equals(userService.getCurrentUser().getLogin())) {
            card.setMinded(true);
            this.cardRepository.save(card);
        }
        return "redirect:/cards";
    }
}
